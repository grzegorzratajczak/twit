package com.sda.twitt;

public class AuthenticationHolder {

    private static boolean isAuthenticated = false;

    public static boolean getIsAuthenticated() {
        return isAuthenticated;
    }

    public static void setIsAuthenticated(boolean isAuthenticated) {
        AuthenticationHolder.isAuthenticated = isAuthenticated;
    }
}
