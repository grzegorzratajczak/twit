package com.sda.twitt;

import javax.servlet.*;
import java.io.IOException;

public class TestFilter2 implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

//        System.out.println("Filter - 2");

        if (request.getParameter("secret123") != null) {
            System.out.println("pass filter - 2");
        }else{
            System.out.println("NOT pass filter - 2");
        }

        chain.doFilter(request, response);

    }

    @Override
    public void destroy() {

    }
}
