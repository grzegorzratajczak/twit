package com.sda.twitt;

import javax.servlet.*;
import java.io.IOException;

public class TestFilter3 implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        String pass = String.valueOf(request.getParameter("secret123"));
        if (pass.equals("123")) {
            System.out.println("pass filter - 3");
        }else {
            System.out.println("NOT pass filter - 3");
        }

        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
