package com.sda.twitt;

public class Message {

    private final String from;
    private String content;

    public Message(String content, String from) {
        this.content = content;
        this.from = from;
    }

    public String getContent() {
        return content;
    }

    public String getFrom() {
        return from;
    }

    public void setContent(String content) {
        this.content = content;
    }


}
