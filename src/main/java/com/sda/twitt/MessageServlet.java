package com.sda.twitt;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = "/message")
public class MessageServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        MessageCreator messageCreator = new MessageCreator();
        String from = "";
        Message message = messageCreator.create(req.getParameter("content"), from);

        req.setAttribute("message", message);

        if(AuthenticationHolder.getIsAuthenticated()) {
            req.getRequestDispatcher("showMessage.jsp").forward(req, resp);
        }else{
            req.getRequestDispatcher("wrong.jsp").forward(req, resp);
            // resp.sendRedirect("showMessage");
        }
    }
}
