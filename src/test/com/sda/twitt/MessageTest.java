package com.sda.twitt;

import org.junit.Assert;
import org.junit.Test;

public class MessageTest {

    @Test
    public void shouldCreateNewMessage() {
        //given
        MessageCreator messageCreator = new MessageCreator();
        String content = "";
        String from = "";
        //when
        Message message = messageCreator.create(content, from);

        //then
        Assert.assertNotNull(message);
    }

    @Test
    public void shouldNewMessageHaveContent() {
        //given
        MessageCreator messageCreator = new MessageCreator();
        String content = "nasza testowa wiadomosc";
        String from="";

        //when
        Message message = messageCreator.create(content, from);

        //then
        Assert.assertEquals(content, message.getContent());
    }

    @Test
    public void shouldNewMessageHaveFrom(){
        //given
        MessageCreator messageCreator = new MessageCreator();
        String content = "";
        String from = "TestNick";

        //when
        Message message = messageCreator.create(content, from);

        //then
        Assert.assertEquals(from, message.getFrom());
    }
}
